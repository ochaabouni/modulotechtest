//
//  ModuloTechTestTests.swift
//  ModuloTechTestTests
//
//  Created by Omar Chaabouni on 02/07/2021.
//

import XCTest
import RxSwift
import RxTest
import RxBlocking

@testable import ModuloTechTest

class ModuloTechTestTests: XCTestCase {

    let bag = DisposeBag()
    
    override class func setUp() {
        let viewModel = DevicesViewModel(ServiceMock())
        viewModel.fetchData()
    }
    
    func testDevicesDataNotEmpty() {
        
        let expectation = XCTestExpectation(description: " devices data source test")
        AppSessionManager.shared.devices.subscribe(onNext: {
            devices in
            XCTAssertNotNil(devices)
            expectation.fulfill()
        }).disposed(by: bag)
        wait(for: [expectation], timeout: 2.0)
    }
    
    func testgoToProfile() {
        let homeCoordinator = HomeCoordinator((UIApplication.shared.delegate as! AppDelegate).window!, navigationController: UINavigationController())
        homeCoordinator.start()
        homeCoordinator.toProfile()
        XCTAssertTrue(homeCoordinator.coordinators.first is ProfileCoordinator)
    }

}
