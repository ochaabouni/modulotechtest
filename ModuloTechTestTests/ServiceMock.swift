//
//  ServiceMock.swift
//  ModuloTechTestTests
//
//  Created by Omar Chaabouni on 08/07/2021.
//

import Foundation
import RxSwift
@testable import ModuloTechTest

class ServiceMock: DevicesService {
    
    override func fetchData() -> Observable<ConfigurationReponse> {
        let bundle = Bundle(for: ServiceMock.self)
        let mockName = "reponse"
        guard let url = bundle.url(forResource: mockName, withExtension: "json"),
            let data = try? Data(contentsOf: url),
            let config = try? JSONDecoder().decode(ConfigurationReponse.self, from: data) else {
            return Observable.error(ServerError.noData)
        }
        
        return Observable.just(config)
    }
}
