//
//  Date+Formatter.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 07/07/2021.
//

import Foundation


extension Date {
    
    func formattedString() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter.string(from: self)
    }
}

extension String {
    
    func toFormattedDate() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter.date(from: self)
    }
}
