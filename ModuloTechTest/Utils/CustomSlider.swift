//
//  CustomSlider.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 05/07/2021.
//

import UIKit
import RxSwift
import RxCocoa

fileprivate class CustomSlider: UISlider {
    
    override func draw(_ rect: CGRect) {
        superview?.draw(rect)
    }
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        var result = super.trackRect(forBounds: bounds)
        result.origin.x = 0
        result.size.width = bounds.size.width
        result.origin.y = result.origin.y - 2.5
        result.size.height = 10
        return result
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: self.bounds.height, height: self.bounds.width)
    }
    
}

class CustomSliderView: UIView {
    
    var valueChanged = PublishSubject<Float>()
    
    var maxtitle: String? {
        didSet {
            maxTitleLabel.text = maxtitle
        }
    }
    var minTitle: String? {
        didSet {
            minTitleLabel.text = minTitle
        }
    }
    var maxValue: Float = 0.0
    var minValue: Float = 0.0 
    var stepValue: Float = 1.0
    var currentValue: Float = 0.0
    var thumbBackgroundColor: UIColor? {
        didSet {
            layoutSubviews()
        }
    }
    var thumbBorderColor: UIColor?
    var trackEmptyColor: UIColor?
    var trackFullColor: UIColor?
    
    
    private var values: [Float] = []
    private var oldValue: Float = 7.0
    
    private var maxTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private var minTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private var slider: CustomSlider = {
        let slider = CustomSlider()
        slider.addTarget(self, action: #selector(handleValueChange(sender:)), for: .valueChanged)
        slider.translatesAutoresizingMaskIntoConstraints = false
        return slider
    }()
    
    @objc func handleValueChange(sender: UISlider) {
        guard stepValue != 1 else {
            valueChanged.onNext(slider.value)
            return
        }
        var newValue = stepValue * floorf((slider.value/stepValue) + 0.5)
        guard newValue != slider.value.rounded() else {
            return
        }
        newValue = max(min(maxValue, newValue), minValue)
        slider.setValue(newValue , animated: false)
        if newValue != maxValue && newValue != minValue {
            newValue = (newValue - minValue).roundTo(x: stepValue) + minValue
        }
        valueChanged.onNext(newValue)
    }
    
    private func getThumbImage() -> UIImage? {
        if  let image = slider.thumbImage(for: .normal)  {
            return image
        }
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        view.clipsToBounds = false
        view.layer.cornerRadius = 12
        view.layer.borderWidth = 6
        view.backgroundColor = self.thumbBackgroundColor
        view.layer.borderColor = self.thumbBorderColor?.cgColor
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0)
        defer { UIGraphicsEndImageContext() }
        if let context = UIGraphicsGetCurrentContext() {
            view.layer.render(in: context)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            return image
        }
        return nil
    }
    
    init() {
        super.init(frame: .zero)
        initViews()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
        setUpViews()
    }
    
    private func initViews() {
        addSubview(minTitleLabel)
        addSubview(slider)
        addSubview(maxTitleLabel)
    }
    
    private func setUpViews() {
        minTitleLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        maxTitleLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        maxTitleLabel.heightAnchor.constraint(lessThanOrEqualToConstant: 20).isActive = true
        maxTitleLabel.heightAnchor.constraint(lessThanOrEqualToConstant: 20).isActive = true
        maxTitleLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        let center = CGPoint(x: self.bounds.width / 2, y: self.bounds.height / 2)
        slider.widthAnchor.constraint(equalToConstant: self.bounds.height - 50).isActive = true
        slider.heightAnchor.constraint(equalToConstant: 50).isActive = true
        let degrees: CGFloat = -90.0
        slider.transform = CGAffineTransform(translationX: center.x - slider.center.x, y: center.y - slider.center.y)
        slider.transform = slider.transform.rotated(by: degrees * CGFloat.pi / 180)
        minTitleLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        slider.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        slider.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        
        slider.maximumValue = maxValue
        slider.minimumValue = minValue
        slider.setThumbImage(getThumbImage(), for: .normal)
        slider.value = currentValue
        oldValue = currentValue
        slider.minimumTrackTintColor = trackEmptyColor
        slider.maximumTrackTintColor = trackFullColor
        layoutSubviews()
        
    }
}

