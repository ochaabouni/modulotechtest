//
//  Int+Temperature.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 04/07/2021.
//

import Foundation

extension Int {
    
    func toPresentableTempurature() -> String {
        if Locale.current.identifier.contains("US") {
            let formatter = MeasurementFormatter()
            formatter.locale = .init(identifier: "en_US")
            formatter.unitStyle = .medium
            return formatter.string(from: .init(value: Double(self), unit: UnitTemperature.celsius))
        }
        return "\(self)°C"
    }
}
