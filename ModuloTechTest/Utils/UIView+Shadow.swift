//
//  UIView+Shadow.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 04/07/2021.
//

import UIKit


extension UIView {
    
    func withShadow() {
        self.layer.shadowColor = ThemeManager.shadowColor?.cgColor
        self.layer.shadowOffset = CGSize(width: -2, height: -3)
        self.layer.shadowRadius = 5.0
        self.layer.shadowOpacity = 1.0
    }
}
