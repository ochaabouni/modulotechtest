//
//  Float+Rounded.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 06/07/2021.
//

import Foundation

extension Float {
    
    func roundTo(x : Float) -> Float {
        return x * Float(Float(self/x).rounded())
    }
}
