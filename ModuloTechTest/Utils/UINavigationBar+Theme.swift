//
//  UINavigationBar+Theme.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 06/07/2021.
//

import UIKit

extension UINavigationBar {
    
    class func applyTheme() {
        UINavigationBar.appearance().isTranslucent = false
        let backBarButtonImage = UIImage(named: "black_left")
        UINavigationBar.appearance().backIndicatorImage = backBarButtonImage
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = backBarButtonImage
        UINavigationBar.appearance().barTintColor = ThemeManager.mainBackgroundColor
        let enabledFontAttributes: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor(named: "navigation_tint") ?? .black,
        ]

        UIBarButtonItem.appearance().setTitleTextAttributes(enabledFontAttributes, for: .normal)
        UIBarButtonItem.appearance().setTitleTextAttributes(enabledFontAttributes, for: .highlighted)
        UIBarButtonItem.appearance().setTitleTextAttributes(enabledFontAttributes, for: .selected)
        
    }
}

extension UIViewController {
    
    func setNavigationBarButton(_ iconName: String, action: Selector, isLeftBarButtonItem: Bool = true) {
        
        let button = UIButton(frame: CGRect(x: -20, y: 0, width: 50, height: 50))
        button.setImage(UIImage(named: iconName), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
        button.addTarget(self, action: action, for: .touchUpInside)
        
        if isLeftBarButtonItem {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        } else {
           self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
        }
    }
}
