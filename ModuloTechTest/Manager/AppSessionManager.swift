//
//  AppSessionManager.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 02/07/2021.
//

import Foundation
import RxSwift

public class AppSessionManager {
    
    static let shared = AppSessionManager()
    private(set) var user: ReplaySubject<User?>
    private(set) var devices: PublishSubject<[Device]>
    
    private var deviceList: [Device] = [] {
        didSet {
            devices.onNext(deviceList)
        }
    }
    
    private var currentUser: User? {
        didSet {
            user.onNext(currentUser)
        }
    }
    
    private init() {
        user = ReplaySubject<User?>.create(bufferSize: 1)
        devices = PublishSubject<[Device]>()
    }
    
    func setDevices(_ devices: [Device]) {
        self.deviceList = devices
    }
    
    func updateDevice(_ device: Device) {
        if let index = deviceList.firstIndex(where: { $0.id == device.id }) {
            deviceList[index] = device
            devices.onNext(deviceList)
        }
    }
    
    func setCurrentUser(_ user: User) {
        self.currentUser = user
    }
    
}
