//
//  TranslationManager.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 03/07/2021.
//

import Foundation


struct TranslationManager {
    
    // dynamic localizing
    static func localize(_ key: String) -> String {
        return NSLocalizedString(key, comment: "")
    }
    
    // Devices
    static let devicesPageTitle = localize("devices_title")
    static let deviceMode = localize("device_mode")
    
    // Profile
    static let profilePageTitle = localize("profile_title")
    static let profileBirthDate = localize("birth_date")
    static let profileSubmit = localize("profile_submit")
    
    // General
    static let genericValidate = localize("generic_validate")
    static let genericCancel = localize("generic_cancel")
    static let retry = localize("retry")
    static let noData = localize("no_data")
    
}
