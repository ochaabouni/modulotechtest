//
//  ThemeManager.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 04/07/2021.
//

import UIKit


struct ThemeManager {
    
    static let mainBackgroundColor = UIColor(named: "main_background")
    static let separatorColor = UIColor(named: "separator")
    static let cellBackgroundColor = UIColor(named: "cell_background")
    static let shadowColor = UIColor(named: "shadow")
    static let buttonTint = UIColor(named: "navigation_tint")
    
    //Slider
    static let sliderThumbBackgroundColor = UIColor(named: "thumb_background")
    static let sliderThumbBorderColor = UIColor(named: "thumb_border")
    static let sliderLowerColor = UIColor(named: "lower_track")
    static let sliderUpperColor = UIColor(named: "upper_track")
}
