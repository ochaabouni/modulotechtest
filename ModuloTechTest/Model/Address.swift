//
//  Address.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 02/07/2021.
//

import Foundation

struct Address: Decodable {
    var city: String
    var postalCode: Int
    var street: String
    var streetCode: String
    var country: String
}
