//
//  Light.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 02/07/2021.
//

import Foundation

class Light: Device {
    var mode: DeviceMode
    var intensity: Int
    
    enum CodingKeys: CodingKey {
        case mode, intensity
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        mode = DeviceMode(rawValue: try container.decode(String.self, forKey: .mode)) ?? .off
        intensity = try container.decode(Int.self, forKey: .intensity)
        try super.init(from: decoder)
    }
}
