//
//  ConfigurationReponse.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 02/07/2021.
//

import Foundation


struct ConfigurationReponse: Decodable {
    var devices: [Device]
    var profile: User
    
    enum CodingKeys: CodingKey {
        case devices, user
    }
    
    init(from decoder: Decoder) throws {
        let deviceTypes = [Heater.self, Light.self, RollerShutter.self]
        let container = try decoder.container(keyedBy: CodingKeys.self)
        devices = []
        var nestedContainer = try container.nestedUnkeyedContainer(forKey: .devices)
        while !nestedContainer.isAtEnd {
            var didAdd = false
            loop: for element in deviceTypes {
                if let item = try? nestedContainer.decode(element) {
                    didAdd = true
                    devices.append(item)
                    break loop
                }
            }
            if !didAdd {
                _ = try? nestedContainer.decode(Device.self)
            }
        }
        profile = try container.decode(User.self, forKey: .user)
    }
}
