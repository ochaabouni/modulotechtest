//
//  RollerShutter.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 02/07/2021.
//

import Foundation


class RollerShutter: Device {
    var position: Int
    
    enum CodingKeys: CodingKey {
        case position
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        position = try container.decode(Int.self, forKey: .position)
        try super.init(from: decoder)
    }
}
