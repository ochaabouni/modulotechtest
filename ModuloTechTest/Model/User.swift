//
//  User.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 02/07/2021.
//

import Foundation

struct User: Decodable {
    var firstName: String
    var lastName: String
    var birthDate: Double
    var address: Address
}
