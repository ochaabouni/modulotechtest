//
//  Device.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 02/07/2021.
//

import Foundation

class Device: Decodable {
    var id: Int
    var deviceName: String
    var productType: String
    
    enum CodingKeys: CodingKey {
        case id, deviceName, productType
    }
}

enum DeviceMode: String {
    case on = "ON"
    case off = "OFF"
}
