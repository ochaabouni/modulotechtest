//
//  Heater.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 02/07/2021.
//

import Foundation

class Heater: Device {
    var mode: DeviceMode
    var temperature: Int
    
    enum CodingKeys: CodingKey {
        case mode, temperature
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        mode = DeviceMode(rawValue: try container.decode(String.self, forKey: .mode)) ?? .off
        temperature = try container.decode(Int.self, forKey: .temperature)
        try super.init(from: decoder)
    }
}
