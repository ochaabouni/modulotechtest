//
//  ProfileCoordinator.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 06/07/2021.
//

import UIKit
import RxSwift

class ProfileCoordinator: Coordinator, Deallocallable {
    
    var navigationController: UINavigationController
    var onDeallocate = PublishSubject<Deallocallable>()
    var coordinators = [DeallocatableCoordinator]()
    let bag = DisposeBag()
    var window: UIWindow
    
    required init(_ window: UIWindow, navigationController: UINavigationController) {
        self.window = window
        self.navigationController = UINavigationController()
    }

    func start() {
        let viewModel = ProfileViewModel()
        let profileViewController = ProfileViewController()
        profileViewController.viewModel = viewModel
        navigationController.viewControllers = [profileViewController]
        navigationController.modalPresentationStyle = .overFullScreen
        self.window.rootViewController?.present(navigationController, animated: true, completion: nil)
        viewModel.onDismiss.subscribe(onCompleted: {
            self.cancel()
        }).disposed(by: bag)
    }
    
    
    func cancel() {
        self.navigationController.dismiss(animated: true) {
            [weak self] in
            guard let self = self else { return }
            self.onDeallocate.onNext(self)
        }
    }
    
}
