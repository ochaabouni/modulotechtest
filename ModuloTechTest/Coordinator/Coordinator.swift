//
//  Coordinator.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 06/07/2021.
//

import UIKit
import RxSwift

protocol Coordinator: class {
    var navigationController: UINavigationController { get set }
    var coordinators: [DeallocatableCoordinator] { get set }
    var window: UIWindow { get }
    var bag: DisposeBag { get }
    
    init(_ window: UIWindow, navigationController: UINavigationController)
    func start()
    func cancel()
    func startCoordinator<T>(_ coordinator: T.Type) where T: DeallocatableCoordinator
}

typealias DeallocatableCoordinator = Coordinator & Deallocallable

extension Coordinator {
    func startCoordinator<T>(_ coordinator: T.Type) where T: DeallocatableCoordinator {
        let childCoordinator = coordinator.init(window, navigationController: navigationController)
        childCoordinator.start()
        coordinators.append(childCoordinator)
        childCoordinator.onDeallocate.subscribe(onNext: { element in
            self.coordinators.removeAll(where: { $0.reference == element.reference })
        }).disposed(by: bag)
    }
}
