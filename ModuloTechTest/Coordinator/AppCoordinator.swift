//
//  AppCoordinator.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 05/07/2021.
//

import UIKit
import RxSwift

class AppCoordinator: Coordinator {
    var navigationController = UINavigationController()
    var coordinators = [DeallocatableCoordinator]()
    var window: UIWindow
    var bag = DisposeBag()
    
    required init(_ window: UIWindow, navigationController: UINavigationController) {
        self.window = window
    }

    func start() {
        startCoordinator(HomeCoordinator.self)
    }
    
    func cancel() { }
}
