//
//  HomeCoordinator.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 02/07/2021.
//

import UIKit
import RxSwift

class HomeCoordinator: Coordinator, Deallocallable {
    
    var navigationController: UINavigationController
    var coordinators = [DeallocatableCoordinator]()
    var window: UIWindow
    let bag = DisposeBag()
    var onDeallocate = PublishSubject<Deallocallable>()
    
    required init(_ window: UIWindow, navigationController: UINavigationController) {
        self.window = window
        self.navigationController = navigationController
    }
    
    func start() {
        let viewModel = DevicesViewModel(DevicesService())
        let viewController = DevicesViewController()
        viewController.viewModel = viewModel
        navigationController.viewControllers = [viewController]
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
        viewModel.onDeviceSelected.asObserver().subscribe(onNext: {
            [weak self] device in
            self?.showSettingPage(device)
        }).disposed(by: bag)
        viewModel.onProfile.subscribe(onNext: {
            self.toProfile()
        }).disposed(by: bag)
    }
    
    func showSettingPage(_ device: Device) {
        let viewModel = DeviceSettingsViewModel()
        let settingsViewController = DeviceSettingsViewController()
        settingsViewController.viewModel = viewModel
        self.navigationController.pushViewController(settingsViewController, animated: true)
        viewModel.device.onNext(device)
        viewModel.onBack.subscribe(onNext: { [weak self] in
            self?.navigationController.popViewController(animated: true)
        }).disposed(by: bag)
    
    }
    
    func toProfile() {
        startCoordinator(ProfileCoordinator.self)
    }
    
    func cancel() {
        
    }
}
