//
//  Deallocallable.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 06/07/2021.
//

import Foundation
import RxSwift

protocol Deallocallable {
    
    var onDeallocate: PublishSubject<Deallocallable> { get }
    var reference: Int { get }
}


extension Deallocallable {
    
    var reference: Int {
        return unsafeBitCast(self, to: Int.self)
    }
}
