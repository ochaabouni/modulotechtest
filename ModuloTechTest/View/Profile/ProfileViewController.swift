//
//  ProfileViewController.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 06/07/2021.
//

import UIKit
import RxSwift

typealias RowContent = (placeHolder: String, image: String, value: String)
class ProfileViewController: UIViewController {
    
    var viewModel: ProfileViewModel!
    var bag = DisposeBag()
    private let datePicker = UIDatePicker()
    private var fields = [ProfileField]()
    
    private var user: User?
    
    lazy private var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = ThemeManager.mainBackgroundColor
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    lazy private var imageContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy private var imageview: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "profile")
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 75
        imageView.layer.borderWidth = 2
        imageView.layer.borderColor = UIColor.gray.cgColor
        imageView.widthAnchor.constraint(equalToConstant: 150).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        return imageView
    }()
    
    lazy private var containerStackView: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.distribution = .equalSpacing
        stack.spacing = 12
        return stack
    }()
    
    lazy private var submitButton: UIButton = {
        let button = UIButton()
        button.isHidden = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
        button.setTitle(TranslationManager.profileSubmit, for: .normal)
        button.setTitleColor(ThemeManager.mainBackgroundColor ?? .white, for: .normal)
        button.setTitleColor(ThemeManager.mainBackgroundColor?.withAlphaComponent(0.3) ?? .white, for: .disabled)
        button.backgroundColor = ThemeManager.buttonTint
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
        setNavigationBarButton("black_cross", action: #selector(dismissView))
        setNavigationBarButton("edit", action: #selector(edit), isLeftBarButtonItem: false)
        self.title = TranslationManager.profilePageTitle
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
        addSubViews()
    }
    
    private func addSubViews() {
        self.view.addSubview(scrollView)
        scrollView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        
        scrollView.addSubview(imageContainer)
        imageContainer.widthAnchor.constraint(equalToConstant: self.view.bounds.width).isActive = true
        imageContainer.heightAnchor.constraint(equalToConstant: 250).isActive = true
        imageContainer.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        imageContainer.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
        imageContainer.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
        
        imageContainer.addSubview(imageview)
        imageview.centerYAnchor.constraint(equalTo: imageContainer.centerYAnchor).isActive = true
        imageview.centerXAnchor.constraint(equalTo: imageContainer.centerXAnchor).isActive = true
        
        scrollView.addSubview(containerStackView)
        containerStackView.isUserInteractionEnabled = true
        containerStackView.topAnchor.constraint(equalTo: imageContainer.bottomAnchor).isActive = true
        containerStackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 24).isActive = true
        containerStackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -24).isActive = true
        containerStackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -50).isActive = true

        viewModel.profile.subscribe(onNext: {
            [weak self] user in
            self?.user = user
            self?.setupUser()
        }).disposed(by: bag)
        
    }
    
    private func setupUser() {
        guard let user = self.user else {
            return
        }
        
        let rows: [RowContent] = [RowContent(placeHolder: "first_name", image: "user_info", value: user.firstName),
                                  RowContent(placeHolder: "last_name", image: "user_info", value: user.lastName),
                                  RowContent(placeHolder: "street_code", image: "address", value: user.address.streetCode),
                                  RowContent(placeHolder: "street", image: "address", value: user.address.street),
                                  RowContent(placeHolder: "postal_code", image: "address", value:  "\(user.address.postalCode)"),
                                  RowContent(placeHolder: "city", image: "country", value: user.address.city),
                                  RowContent(placeHolder: "country", image: "country", value: user.address.country),
                                  RowContent(placeHolder: "birth_date", image: "birth_date", value: Date(timeIntervalSince1970: user.birthDate / 1000).formattedString() ?? "")]
        if self.fields.isEmpty {
            var textFieldsObservers: [Observable<Bool>] = []
            
            rows.forEach {
                let field = ProfileField(frame: .zero)
                field.value = $0.value
                field.identifier = $0.placeHolder
                field.imageName = $0.image
                field.isEditing = false
                field.isUserInteractionEnabled = true
                field.heightAnchor.constraint(equalToConstant: 50).isActive = true
                field.keyboardType = $0.placeHolder == "postal_code" ? .numberPad : .default
                self.containerStackView.addArrangedSubview(field)
                if $0.placeHolder == "birth_date" {
                    field.valueTextField.addTarget(self, action: #selector(openDatePicker(_:)), for: .editingDidBegin)
                }
                let obs = field.valueTextField.rx.text.map({ !($0?.isEmpty ?? false) }).share()
                textFieldsObservers.append(obs)
                fields.append(field)
            }
            Observable.combineLatest(textFieldsObservers) { value -> Bool in
                return !value.contains(false)
            }.bind(to: submitButton.rx.isEnabled)
            .disposed(by: bag)
            
            self.containerStackView.addArrangedSubview(submitButton)
            submitButton.addTarget(self, action: #selector(self.saveUser), for: .touchUpInside)
        } else {
            for (index, element) in rows.enumerated() {
                fields[index].value = element.value
                fields[index].identifier = element.placeHolder
                fields[index].imageName = element.image
                fields[index].isEditing = false
            }
        }
        
    }
    
    @objc func edit() {
        self.view.endEditing(true)
        let isEditing = fields.first?.isEditing ?? false
        fields.forEach({ $0.isEditing = !isEditing })
        submitButton.isHidden = !submitButton.isHidden
        if !isEditing {
            setNavigationBarButton("black_cross", action: #selector(edit), isLeftBarButtonItem: false)
            
        } else {
            setNavigationBarButton("edit", action: #selector(edit), isLeftBarButtonItem: false)
            fields.forEach {
                $0.value = $0.value
            }
        }
    }
    
    @objc func saveUser() {
        self.view.endEditing(true)
        self.submitButton.isHidden = true
        setNavigationBarButton("edit", action: #selector(edit), isLeftBarButtonItem: false)
        fields.forEach { field in
            switch field.identifier {
            case  "first_name":
                self.user?.firstName = field.value ?? ""
            case "last_name":
                self.user?.lastName = field.value ?? ""
            case "street_code":
                self.user?.address.streetCode = field.value ?? ""
            case "street":
                self.user?.address.street = field.value ?? ""
            case "postal_code":
                if let postalCode = Int(field.value ?? "") {
                    self.user?.address.postalCode = postalCode
                }
            case "city":
                self.user?.address.city = field.value ?? ""
            case "country":
                self.user?.address.country = field.value ?? ""
            case "birth_date":
                if let date = field.value?.toFormattedDate() {
                    self.user?.birthDate = date.timeIntervalSince1970 * 1000
                }
            default:
                break
            }
        }
        
        guard let user = self.user else {
            return
        }
        viewModel.onProfileUpdate.onNext(user)
    }

    @objc func dismissView() {
        viewModel.onDismiss.onCompleted()
    }
    
    @objc private func openDatePicker(_ sender: UITextField) {
        datePicker.maximumDate = Date()
        if #available(iOS 14, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        datePicker.datePickerMode = .date
        if let field =  fields.filter({ $0.identifier == "birth_date" }).first,
           let date = field.value?.toFormattedDate(),
           field.isEditing {
            datePicker.date = date
        } else {
            datePicker.setDate(Date(timeIntervalSince1970: user?.birthDate ?? 0 / 1000), animated: true)
        }
        sender.inputView = datePicker
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: TranslationManager.genericValidate, style: .plain, target: self, action: #selector(didSelectDate))
        let cancelButton = UIBarButtonItem(title: TranslationManager.genericCancel, style: .plain, target: self, action: #selector(cancelDateSelection))
        let pickerTitle = UIBarButtonItem(title: TranslationManager.profileBirthDate, style: .plain, target: self, action: nil)

        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        toolBar.setItems([cancelButton, flexible, pickerTitle, flexible, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        sender.inputAccessoryView = toolBar
    }
    
    @objc private func didSelectDate() {
        view.endEditing(true)
        let selectedDate = datePicker.date
        fields.filter({ $0.identifier == "birth_date" }).first?.value = selectedDate.formattedString()
    }
    @objc private func cancelDateSelection() {
        view.endEditing(true)
    }
}

extension ProfileViewController {
    @objc func keyboardWillShow(notification:NSNotification) {

        guard let userInfo = notification.userInfo else { return }
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)

        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height + 20
        scrollView.contentInset = contentInset
    }

    @objc func keyboardWillHide(notification:NSNotification) {

        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
}
