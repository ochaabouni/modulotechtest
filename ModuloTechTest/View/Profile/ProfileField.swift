//
//  ProfileField.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 07/07/2021.
//

import UIKit

class ProfileField: UIView {
    
    var value: String? {
        get {
            return isEditing ? valueTextField.text : valueLabel.text
        }
        set {
            valueLabel.text = newValue
            valueTextField.text = newValue
        }
    }
    var imageName: String = "" {
        didSet {
            imageView.image = UIImage(named: imageName)
        }
    }
    var identifier: String = "" {
        didSet {
            placeHolder.text = TranslationManager.localize(identifier)
        }
    }
    
    var isEditing: Bool = false {
        didSet {
            isEditing(isEditing)
        }
    }
    
    var keyboardType: UIKeyboardType = .default {
        didSet {
            valueTextField.keyboardType = keyboardType
        }
    }
    
    
    lazy private var stackView: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .horizontal
        stack.distribution = .fillProportionally
        stack.spacing = 12
        return stack
    }()
    
    lazy private var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.widthAnchor.constraint(equalToConstant: 15).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 15).isActive = true
        return imageView
    }()
    
    lazy private var placeHolder: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.widthAnchor.constraint(equalToConstant: 100).isActive = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy private var valueLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy private(set) var valueTextField: UITextField = {
        let textField = UITextField()
        textField.borderStyle = .roundedRect
        textField.autocorrectionType = .no
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private func addSubViews() {
        addSubview(stackView)
        stackView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        stackView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        
        stackView.addArrangedSubview(imageView)
        stackView.addArrangedSubview(placeHolder)
        stackView.addArrangedSubview(valueLabel)
        stackView.addArrangedSubview(valueTextField)
        valueLabel.isHidden = isEditing
        valueTextField.isHidden = !isEditing
        
    }
    
    private func isEditing(_ bool: Bool) {

        UIView.animate(withDuration: 0.2, delay: 0.0, options: [.curveEaseIn], animations: {
            self.valueLabel.isHidden = bool
            self.valueLabel.alpha = bool ? 0 : 1
            self.valueTextField.alpha = bool ? 1 : 0
            self.valueTextField.isHidden = !bool
        })
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
