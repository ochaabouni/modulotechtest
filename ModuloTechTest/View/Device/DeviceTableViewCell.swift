//
//  DeviceTableViewCell.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 03/07/2021.
//

import UIKit
 
class DeviceTableViewCell: UITableViewCell {
    
    static let identifier = "DeviceTableViewCell"
    
    var device: Device? {
        didSet {
            self.configureDevice()
        }
    }
    
    lazy private var deviceImageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.widthAnchor.constraint(equalToConstant: 25).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 25).isActive = true
        return imageView
    }()
    
    lazy private var deviceNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()
    
    lazy private var container: UIView = {
        let container = UIView()
        container.withShadow()
        container.backgroundColor = ThemeManager.cellBackgroundColor
        container.translatesAutoresizingMaskIntoConstraints = false
        
        return container
    }()
    
    lazy private var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = NSLayoutConstraint.Axis.horizontal
        stackView.spacing = 5
        stackView.distribution = .fillProportionally
        return stackView
    }()
    
    lazy private var valuesStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = NSLayoutConstraint.Axis.horizontal
        stackView.spacing = 2
        stackView.distribution = .fillProportionally
        stackView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width / 4).isActive = true
        return stackView
    }()
    
    lazy private var descriptionView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy private var separatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.widthAnchor.constraint(equalToConstant: 1).isActive = true
        view.backgroundColor = ThemeManager.separatorColor
        
        return view
    }()
    
    lazy private var valueLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 22)
        label.text = "22.5°C"
        label.textAlignment = .center
        return label
    }()
    
    lazy private var statusImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.widthAnchor.constraint(equalToConstant: 15).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 15).isActive = true
        return imageView
    }()
    
    lazy private var statusView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.widthAnchor.constraint(equalToConstant: 20).isActive = true
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    private func setupViews() {
        self.backgroundColor = .clear
        addSubview(container)
        container.topAnchor.constraint(equalTo: topAnchor, constant: 12).isActive = true
        container.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -12).isActive = true
        container.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12).isActive = true
        container.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12).isActive = true
        container.heightAnchor.constraint(greaterThanOrEqualToConstant: 100).isActive = true
        container.layer.cornerRadius = 6
        
        // container
        container.addSubview(stackView)
        stackView.topAnchor.constraint(equalTo: container.topAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
        stackView.leadingAnchor.constraint(equalTo: container.leadingAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: container.trailingAnchor).isActive = true
        
        //stackview
        stackView.addArrangedSubview(descriptionView)
        stackView.addArrangedSubview(separatorView)
        stackView.addArrangedSubview(valuesStackView)
        
        separatorView.topAnchor.constraint(equalTo: stackView.topAnchor, constant: 6).isActive = true
        separatorView.bottomAnchor.constraint(equalTo: stackView.bottomAnchor, constant: -6).isActive = true
        
        //descriptionView
        descriptionView.addSubview(deviceImageView)
        deviceImageView.leadingAnchor.constraint(equalTo: descriptionView.leadingAnchor, constant: 24).isActive = true

        deviceImageView.centerYAnchor.constraint(equalTo: descriptionView.centerYAnchor, constant: 0).isActive = true
        
        descriptionView.addSubview(deviceNameLabel)
        deviceNameLabel.leadingAnchor.constraint(equalTo: deviceImageView.trailingAnchor, constant: 16).isActive = true
        deviceNameLabel.trailingAnchor.constraint(equalTo: descriptionView.trailingAnchor, constant: -25).isActive = true
        deviceNameLabel.centerYAnchor.constraint(equalTo: descriptionView.centerYAnchor, constant: 0).isActive = true
        
        // values
        valuesStackView.addArrangedSubview(statusView)
        valuesStackView.addArrangedSubview(valueLabel)
        statusView.addSubview(statusImageView)
        statusImageView.centerXAnchor.constraint(equalTo: statusView.centerXAnchor).isActive = true
        statusImageView.centerYAnchor.constraint(equalTo: statusView.centerYAnchor).isActive = true
        
    }
    
    private func configureDevice() {
        guard let device = device else {
            return
        }
        deviceNameLabel.text = device.deviceName
        deviceImageView.image = UIImage(named: device.productType)
        
        switch device {
        case is Heater:
            if let heater = device as? Heater {
                valueLabel.text = heater.temperature.toPresentableTempurature()
                statusImageView.image = UIImage(named: heater.mode.rawValue)
                statusView.isHidden = false
            }
        case is Light:
            if let light = device as? Light {
                valueLabel.text = "\(light.intensity)%"
                statusImageView.image = UIImage(named: light.mode.rawValue)
                statusView.isHidden = false
            }
        case is RollerShutter:
            if let roller = device as? RollerShutter {
                valueLabel.text = "\(roller.position)%"
                statusView.isHidden = true
            }
        default:
            statusView.isHidden = false
        }
    }
    
    override func prepareForReuse() {
        self.configureDevice()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
