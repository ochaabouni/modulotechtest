//
//  DevicesViewController.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 02/07/2021.
//

import UIKit
import RxSwift
import RxCocoa

class DevicesViewController: UIViewController {
    
    var viewModel: DevicesViewModel!
    var bag = DisposeBag()
    
    private var dataSource = [String: [Device]]()
    private var sectionsDataSource = [String]()
    
    lazy private var tableView: UITableView = {
        let tableView = UITableView(frame: self.view.frame, style: .grouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 150
        tableView.register(DeviceTableViewCell.self, forCellReuseIdentifier: DeviceTableViewCell.identifier)
        tableView.backgroundColor = ThemeManager.mainBackgroundColor
        return tableView
    }()
    
    lazy private var errorView: UIView = {
        let view = UIView()
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        let imageView = UIImageView(image: UIImage(named: "ghost"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        view.addSubview(imageView)
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -40).isActive = true
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        label.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 12).isActive = true
        label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -24).isActive = true
        label.text = TranslationManager.noData
        label.numberOfLines = 0
        label.textAlignment = .center
        
        let retryButton = UIButton()
        retryButton.translatesAutoresizingMaskIntoConstraints = false
        retryButton.setTitle(TranslationManager.retry, for: .normal)
        retryButton.setTitleColor(ThemeManager.buttonTint, for: .normal)
        retryButton.addTarget(self, action: #selector(self.requestData), for: .touchUpInside)
        view.addSubview(retryButton)
        retryButton.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 12).isActive = true
        retryButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViews()
        requestData()
    }
    
    private func initViews() {
        
        self.title = TranslationManager.devicesPageTitle
        self.view.addSubview(tableView)
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        self.view.addSubview(errorView)
        errorView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        errorView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        errorView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        errorView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        setNavigationBarButton("user", action: #selector(onProfileSelected), isLeftBarButtonItem: false)
        
        viewModel.devices.subscribe(onNext: { [weak self] devices in
            guard let self = self else { return }
            self.setDataSource(devices)
            DispatchQueue.main.async {
                self.errorView.isHidden = !devices.isEmpty
            }
        }, onError: { _ in }).disposed(by: bag)
        self.view.setNeedsLayout()
    }
    
    @objc private func requestData() {
        viewModel.fetchData()
    }
    
    
    private func setDataSource(_ dataSource: [String: [Device]]) {
        self.dataSource = dataSource
        self.sectionsDataSource = dataSource.keys.compactMap({ $0 }).sorted(by: { (lhs, rhs) -> Bool in
            return lhs > rhs
        })
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    private func getHeader(_ title: String) -> UIView {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 70.0))
        let headerLabel = UILabel()
        headerView.addSubview(headerLabel)
        headerLabel.translatesAutoresizingMaskIntoConstraints = false
        headerLabel.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        headerLabel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 33).isActive = true
        headerLabel.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: 33).isActive = true
        headerLabel.sizeToFit()
        headerLabel.font = UIFont.boldSystemFont(ofSize: 17)
        headerLabel.text = title
        return headerView
    }
    
    @objc func onProfileSelected() {
        viewModel.onProfile.onNext(())
    }
    
}

extension DevicesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: DeviceTableViewCell.identifier) as? DeviceTableViewCell {
//            let index = dataSource.index(dataSource.startIndex, offsetBy: indexPath.section)
            let key = sectionsDataSource[indexPath.section]
            cell.device = dataSource[key]?[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionsDataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let key = sectionsDataSource[section]
        return dataSource[key]?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let key = sectionsDataSource[section]
        return getHeader(TranslationManager.localize(key))
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let key = sectionsDataSource[indexPath.section]
        guard let device = dataSource[key]?[indexPath.row] else {
            return
        }
        viewModel.onDeviceSelected.onNext(device)
    }
}

