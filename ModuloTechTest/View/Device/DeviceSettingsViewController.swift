//
//  DeviceSettingsViewController.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 05/07/2021.
//

import UIKit
import RxSwift

class DeviceSettingsViewController: UIViewController {
    
    var viewModel: DeviceSettingsViewModel!
    var bag = DisposeBag()
    private var device: Device?

    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBarButton("black_left", action: #selector(onBackClicked(sender:)))
        
        viewModel.device.subscribe(onNext: {
            [weak self] device in
            self?.device = device
            self?.setupDevice()
        }).disposed(by: bag)

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupview()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    lazy private var slider: CustomSliderView = {
        let slider = CustomSliderView()
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.heightAnchor.constraint(equalToConstant: self.view.bounds.height / 2).isActive = true
        slider.thumbBorderColor = ThemeManager.sliderThumbBorderColor
        slider.thumbBackgroundColor = ThemeManager.sliderThumbBackgroundColor
        slider.trackFullColor = ThemeManager.sliderUpperColor
        slider.trackEmptyColor = ThemeManager.sliderLowerColor 
        return slider
    }()
    
    lazy private var container: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.spacing = 20
        view.distribution = .fillProportionally
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    lazy private var header: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 50).isActive = true
        return view
    }()
    
    lazy private var statusLabel: UILabel = {
        let label = UILabel()
        label.text = TranslationManager.deviceMode
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy private var statusSwitch: UISwitch = {
        let switsh = UISwitch()
        switsh.translatesAutoresizingMaskIntoConstraints = false
        return switsh
    }()
    
    private func setupview() {
        view.backgroundColor = ThemeManager.mainBackgroundColor
        view.addSubview(container)
        container.topAnchor.constraint(equalTo: view.topAnchor, constant: self.view.safeAreaInsets.top + 24).isActive = true
        container.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        container.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -24).isActive = true
        
        header.addSubview(statusLabel)
        header.addSubview(statusSwitch)
        statusLabel.leadingAnchor.constraint(equalTo: header.leadingAnchor, constant: 0).isActive = true
        statusLabel.centerYAnchor.constraint(equalTo: header.centerYAnchor).isActive = true
        
        statusSwitch.trailingAnchor.constraint(equalTo: header.trailingAnchor, constant: 0).isActive = true
        statusSwitch.centerYAnchor.constraint(equalTo: header.centerYAnchor).isActive = true
        statusSwitch.addTarget(self, action: #selector(onSwitch(_:)), for: .valueChanged)
        container.addArrangedSubview(header)
        
        container.addArrangedSubview(slider)
    }
    
    private func setupDevice() {
        guard let device = self.device else { return }
        self.title = device.deviceName
        switch device {
        case is Light:
            guard let light = device as? Light else { return }
            statusSwitch.isOn = light.mode == .on
            slider.alpha = statusSwitch.isOn ? 1.0 : 0.5
            slider.isUserInteractionEnabled = statusSwitch.isOn
            slider.currentValue = Float(light.intensity)
            slider.maxtitle = "100"
            slider.minTitle = "0"
            slider.maxValue = 100
            slider.minValue = 0
            slider.stepValue = 10
        case is Heater:
            guard let heater = device as? Heater else { return }
            statusSwitch.isOn = heater.mode == .on
            slider.alpha = statusSwitch.isOn ? 1.0 : 0.5
            slider.isUserInteractionEnabled = statusSwitch.isOn
            slider.currentValue = Float(heater.temperature)
            slider.maxtitle = "28"
            slider.minTitle = "7"
            slider.maxValue = 28
            slider.minValue = 7
            slider.stepValue = 5
        case is RollerShutter:
            guard let roller = device as? RollerShutter else { return }
            header.isHidden = true
            slider.currentValue = Float(roller.position)
            slider.maxtitle = "100"
            slider.minTitle = "0"
            slider.maxValue = 100
            slider.minValue = 0
            slider.stepValue = 1
        default:
            break
        }
        
        slider.valueChanged.debounce(.milliseconds(300), scheduler: MainScheduler.asyncInstance).subscribe(onNext: { [weak self] value in
            guard let device = self?.device else { return }
            switch device {
            case is Light:
                (self?.device as? Light)?.intensity = Int(value)
                
            case is RollerShutter:
                (self?.device as? RollerShutter)?.position = Int(value)
                
            case is Heater:
                (self?.device as? Heater)?.temperature = Int(value)
                
            default:
                break
            }
            self?.viewModel.onDeviceChanged.onNext(device)
        }).disposed(by: bag)
    }
    

    @objc private func onSwitch(_ sender: Any) {
        guard let device = self.device else { return }
        slider.alpha = statusSwitch.isOn ? 1.0 : 0.5
        slider.isUserInteractionEnabled = statusSwitch.isOn
        switch device {
        case is Light:
            (self.device as? Light)?.mode = statusSwitch.isOn ? .on : .off
            
        case is Heater:
            (self.device as? Heater)?.mode = statusSwitch.isOn ? .on : .off
            
        default:
            break
        }
        self.viewModel.onDeviceChanged.onNext(device)
    }
    
    @objc func onBackClicked(sender: Any) {
        viewModel.onBack.onNext(())
    }
}
