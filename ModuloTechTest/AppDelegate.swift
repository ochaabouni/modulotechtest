//
//  AppDelegate.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 02/07/2021.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?
    var appCoordinator: AppCoordinator?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UINavigationBar.applyTheme()
        
        window = UIWindow()
        
        appCoordinator = AppCoordinator(window!, navigationController: UINavigationController())
        appCoordinator?.start()
        
        return true
    }


}

