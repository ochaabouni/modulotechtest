//
//  ServerError.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 02/07/2021.
//

import Foundation


enum ServerError: Error {
    case noData
    case malformedUrl
}
