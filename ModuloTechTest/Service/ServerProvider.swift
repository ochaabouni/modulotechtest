//
//  ServerProvider.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 02/07/2021.
//

import Foundation
import RxSwift

protocol ServerProvider {
    func fetchData() -> Observable<ConfigurationReponse>
}
