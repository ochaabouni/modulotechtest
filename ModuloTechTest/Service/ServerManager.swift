//
//  ServerManager.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 02/07/2021.
//

import Foundation
import RxSwift
import RxCocoa

class ServerManager: ServerProvider {
    
    static let shared: ServerProvider = ServerManager()
    
    private let baseUrl = "http://storage42.com/modulotest/"
    private init() {}
    
    func fetchData() -> Observable<ConfigurationReponse> {
        
        guard let url = URL(string: "\(baseUrl)data.json") else {
            return Observable.error(ServerError.malformedUrl)
        }
        
        let request = URLRequest(url: url)
        return Observable.create { obs in
            URLSession.shared.rx.response(request: request).subscribe(
                onNext: { response in
                    guard let responseData = try? JSONDecoder().decode(ConfigurationReponse.self, from: response.data) else {
                        obs.onError(ServerError.noData)
                        return
                    }
                    obs.onNext(responseData)
                },
                onError: {error in
                    obs.onError(error)
                })
        }
    }
}
