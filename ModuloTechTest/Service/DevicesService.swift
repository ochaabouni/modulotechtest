//
//  DevicesService.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 08/07/2021.
//

import Foundation
import RxSwift

class DevicesService {
    
    let serverManager = ServerManager.shared
    
    func fetchData() -> Observable<ConfigurationReponse> {
        return serverManager.fetchData()
    }
}
