//
//  DeviceSettingsViewModel.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 05/07/2021.
//

import Foundation
import RxSwift

class DeviceSettingsViewModel {
    
    var device = BehaviorSubject<Device?>(value: nil)
    var onDeviceChanged = PublishSubject<Device>()
    var onBack = PublishSubject<Void>()
    var bag = DisposeBag()
    
    
    init() {
        onDeviceChanged.asObserver().subscribe(onNext: {
            device in
            AppSessionManager.shared.updateDevice(device)
        }).disposed(by: bag)
    }
}
