//
//  ProfileViewModel.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 06/07/2021.
//

import Foundation
import RxSwift

class ProfileViewModel {
    
    private(set) var profile = ReplaySubject<User>.create(bufferSize: 1)
    private(set) var onDismiss = PublishSubject<Void>()
    private(set) var onProfileUpdate = PublishSubject<User>()
    let bag = DisposeBag()
    
    init() {
        AppSessionManager.shared.user.subscribe(onNext: {
            user in
            guard let user = user else { return}
            self.profile.onNext(user)
        }).disposed(by: bag)
        
        onProfileUpdate.subscribe(onNext: {
            user in
            AppSessionManager.shared.setCurrentUser(user)
        }).disposed(by: bag)
    }
}
