//
//  DevicesViewModel.swift
//  ModuloTechTest
//
//  Created by Omar Chaabouni on 02/07/2021.
//

import Foundation
import RxSwift

class DevicesViewModel {
    
    var onDeviceSelected = PublishSubject<Device>()
    var onProfile: PublishSubject<Void>
    var devices: Observable<[String: [Device]]>
    let bag = DisposeBag()
    let service: DevicesService
    
    init(_ service: DevicesService) {
        self.service = service
        onProfile = PublishSubject<Void>()
        
        devices = AppSessionManager.shared.devices.map({ list -> [String: [Device]] in
            let devices: [String: [Device]] = Dictionary(grouping: list, by: { $0.productType })
            return devices
        })
        
    }
    
    func fetchData() {
        
        service.fetchData().subscribe(onNext: { response in
            AppSessionManager.shared.setDevices(response.devices)
            AppSessionManager.shared.user.onNext(response.profile)
        }, onError: { error in
            AppSessionManager.shared.devices.onNext([])
            AppSessionManager.shared.user.onNext(nil)
        }).disposed(by: bag)
    }

}
